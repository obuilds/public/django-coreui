from django.apps import AppConfig


class CoreUiConfig(AppConfig):
    name = "coreui"
    label = "coreui"
