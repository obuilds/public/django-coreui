from setuptools import setup, find_packages


setup(
    name="coreui",
    version="v3.3.0",
    url="https://gitlab.com/obuilds/public/django-coreui",
    author="oBuilds, LLC",
    author_email="k@obuilds.com",
    description="A Django app which includes static files for CoreUI.",
    packages=find_packages(),
    include_package_data=True,
)
